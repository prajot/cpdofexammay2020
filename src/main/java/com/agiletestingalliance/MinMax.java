package com.agiletestingalliance;

public class MinMax {

	public int foo(int param1, int param2) {
		if (param2 > param1) {
			return param2;
		}
		else {
			return param1;
		}
	}

	
	public String bar(String string) {
		if (string!=null && !"".equals(string)) {
			return string;
		}
		if (string==null || "".equals(string)) {
			return string;
		}
		return string;
	}
	

}
