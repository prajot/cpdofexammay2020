package com.agiletestingalliance;

import static org.junit.Assert.*;

import org.junit.Test;

public class DurationTest {

	@Test
	public void testDur() {
		Duration duration = new Duration();
		assertTrue(duration.dur().startsWith("CP-DOF "));
	}

	@Test
	public void testCalculateIntValue() {
		Duration duration = new Duration();
		duration.calculateIntValue();
	}

}
