package com.agiletestingalliance;

import static org.junit.Assert.*;

import org.junit.Test;

public class MinMaxTest {

	@Test
	public void testF() {
		MinMax max = new MinMax();
		assertTrue(2 == max.foo(1, 2));
		assertTrue(3 == max.foo(3, 2));
	}

	@Test
	public void testBar() {
		MinMax max = new MinMax();
		assertTrue("abc".equalsIgnoreCase(max.bar("abc")));
	}

}
