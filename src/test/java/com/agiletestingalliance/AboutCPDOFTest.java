package com.agiletestingalliance;

import static org.junit.Assert.*;

import org.junit.Test;

public class AboutCPDOFTest {

	@Test
	public void testDesc() {
		AboutCPDOF aboutCPDOF = new AboutCPDOF();
		assertTrue(aboutCPDOF.desc().startsWith("CP-DOF"));
	}

}
